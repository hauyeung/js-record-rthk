const spawnSync = require("child_process").spawnSync;
require("dotenv").config();
const moment = require("moment");
const Handlebars = require("handlebars");
const path = require("path");

const {
  START_DATE,
  END_DATE,
  SHOW_URL: showUrl,
  SHOW_NAME: showName,
  DOWNLOAD_PATH: downloadPath,
  EXTENSION: extension,
} = process.env;
const showInterval = +process.env.SHOW_INTERVAL;
const startDate = moment(START_DATE, "YYYYMMDD");
const endDate = moment(END_DATE, "YYYYMMDD");
const ffmpegPath = path.join("C:", "ffmpeg", "bin", "ffmpeg");

if (!showUrl.includes(showName)) {
  console.log("Show name doesn't match URL");
  process.exit();
}

let dates = [START_DATE];
let date = startDate;
while (+startDate.toDate() < +endDate.toDate()) {
  date = date.add(showInterval, "days");
  const formattedDate = date.format("YYYYMMDD");
  dates.push(formattedDate);
}

for (let date of dates) {
  try {
    const template = Handlebars.compile(showUrl);
    const fullUrl = template({ date });
    console.log(`Downloading ${fullUrl}`);
    const destinationPath = path.join(
      downloadPath,
      `${showName}${date}.${extension}`
    );
    spawnSync(
      `${ffmpegPath} -i "${fullUrl}" -acodec copy -crf 35 -preset ultrafast "${destinationPath}"`,
      {
        stdio: "inherit",
        shell: true,
      }
    );
  } catch (error) {
    console.log(error);
  }
}
